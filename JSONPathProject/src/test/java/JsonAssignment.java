import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.jayway.jsonpath.Criteria;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.Filter;
import com.jayway.jsonpath.JsonPath;
import bsh.ParseException;
import net.minidev.json.parser.JSONParser;

public class JsonAssignment {
	String jsonData;

	@BeforeTest
	public void readJsonFile() throws FileNotFoundException, IOException, ParseException, Exception {
		JSONParser jsonParser = new JSONParser();
		Object object = jsonParser.parse(new FileReader(System.getProperty("user.dir") + "\\src\\test\\resources\\sample-data.json"));
		jsonData = object.toString();
	}

	@Test(enabled = true, priority = 1, description = "Display all users name")
	public void displayAllUsersName() {
		String jsonPath = "$.[*]name";
		DocumentContext jsonContext = JsonPath.parse(jsonData);
		List<String> jsonRead = jsonContext.read(jsonPath);
		int count = 1;
		for (String name : jsonRead) {
			System.out.println( count +". User Name " +" - " + name);
			count++;
		}
	}

	@Test(enabled = true, priority = 2, description = "Display all female users")
	public void displayAllFemaleUsers() {
		String jsonPath = "$.[?(@.gender == 'female')].name";
		DocumentContext jsonContext = JsonPath.parse(jsonData);
		List<String> jsonRead = jsonContext.read(jsonPath);
		int count = 1;
		for (String Fname : jsonRead) {
			System.out.println( count +". Female User " + " - " + Fname);
			count++;
		}
	}

	@Test(enabled = true, priority = 3, description = "Display first two users only")
	public void displayFirstTwoUsers() {
		String jsonPath = "$.[:2].name";
		DocumentContext jsonContext = JsonPath.parse(jsonData);
		List<String> jsonRead = jsonContext.read(jsonPath);
		int count = 1;
		for (int i = 0; i < jsonRead.size(); i++) {
			System.out.println( count +". User " +" - " +jsonRead.get(i));
			count++;
		}
	}

	@Test(enabled = true, priority = 4, description = "Display last two users only")
	public void displayLastTwoUsers() {
		String lasttwouser = "$.[-2:].name";
		DocumentContext jsonContext = JsonPath.parse(jsonData);
		List<String> jsonRead = jsonContext.read(lasttwouser);
		int count = 1;
		for (String firstName : jsonRead) {
			System.out.println( count +". Last User " + " - "  + firstName);
			count++;
		}
	}

	@Test(enabled = true, priority = 5, description = "Display total number of friends for first user")
	public void displayTotalFriendsForFirstUser() {
		String jsonPath = "$.[0].friends";
		DocumentContext jsonContext = JsonPath.parse(jsonData);
		List<String> jsonRead = jsonContext.read(jsonPath);
		System.out.println("Total number of friends for first user - "+jsonRead.size());
	}

	@Test(enabled = true, priority = 6, description = "Display all users whose age between 24 to 36")
	public void displayUsersAgeBtwn() {
		String ageRangeList = "$..[?(@.age>=24 && @.age<=36)].name";
		DocumentContext jsonContext = JsonPath.parse(jsonData);
		List<String> jsonRead = jsonContext.read(ageRangeList);
		for (String ageRange : jsonRead) {
			System.out.println("Users with age between 24 to 36 - " + ageRange);
		}
	}

	@Test(enabled = true, priority = 7, description = "Display balance of male users")
	public void displayBalanceOfMaleUsers() {
		String jsonPath = "$..[?(@.gender == 'male')].balance";
		DocumentContext jsonContext = JsonPath.parse(jsonData);
		List<String> jsonRead = jsonContext.read(jsonPath);
		int count = 1;
		for (String balance : jsonRead) {
			System.out.println("Display balance of male users " + count + " - " + balance);
			count++;
		}
	}
	
	@Test(enabled = true, priority = 8, description = "Display all users who lives in California")
	public void displayAllUsersInCalifornia() {
		Filter californiaUser = Filter.filter(Criteria.where("address").contains("California"));
		DocumentContext jsonContext = JsonPath.parse(jsonData);
		List<Map<String, Object>> jsonread = jsonContext.read("$[*][?]", californiaUser);
		for (int iTemp = 0; iTemp < jsonread.size(); iTemp++) {
			String userName = (String) jsonread.get(iTemp).get("name");
			System.out.println("Users who live in California is - " + userName);
		}
	}
}
